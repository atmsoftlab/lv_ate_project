﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Hardware Abstraction Layer" Type="Folder">
			<Item Name="DAQ" Type="Folder">
				<Item Name="API" Type="Folder"/>
				<Item Name="Demo" Type="Folder"/>
				<Item Name="DAQ HAL (2).aliases" Type="Document" URL="../Hardware Abstraction Layer/DAQ/DAQ HAL (2).aliases"/>
				<Item Name="DAQ HAL.aliases" Type="Document" URL="../Hardware Abstraction Layer/DAQ/DAQ HAL.aliases"/>
				<Item Name="DAQ HAL.lvlps" Type="Document" URL="../Hardware Abstraction Layer/DAQ/DAQ HAL.lvlps"/>
				<Item Name="DAQ HAL.lvproj" Type="Document" URL="../Hardware Abstraction Layer/DAQ/DAQ HAL.lvproj"/>
				<Item Name="DAQ.lvlib" Type="Library" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlib"/>
				<Item Name="DAQ.lvlibp" Type="LVLibp" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp">
					<Item Name="BuildHelpPath.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
					<Item Name="Check Special Tags.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
					<Item Name="DAQ.lvclass" Type="LVClass" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/Hardware Abstraction Layer/DAQ/DAQ.lvclass"/>
					<Item Name="DAQmx Clear Task.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
					<Item Name="DAQmx Connect Terminals.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/routing.llb/DAQmx Connect Terminals.vi"/>
					<Item Name="DAQmx Control Task.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/task.llb/DAQmx Control Task.vi"/>
					<Item Name="DAQmx Create AI Channel (sub).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create AI Channel (sub).vi"/>
					<Item Name="DAQmx Create AO Channel (sub).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create AO Channel (sub).vi"/>
					<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
					<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
					<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
					<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
					<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
					<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
					<Item Name="DAQmx Create CO Channel (sub).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create CO Channel (sub).vi"/>
					<Item Name="DAQmx Create DI Channel (sub).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create DI Channel (sub).vi"/>
					<Item Name="DAQmx Create DO Channel (sub).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Create DO Channel (sub).vi"/>
					<Item Name="DAQmx Disconnect Terminals.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/routing.llb/DAQmx Disconnect Terminals.vi"/>
					<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
					<Item Name="DAQmx Is Task Done.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/task.llb/DAQmx Is Task Done.vi"/>
					<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
					<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
					<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
					<Item Name="DAQmx Rollback Channel If Error.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/create/channels.llb/DAQmx Rollback Channel If Error.vi"/>
					<Item Name="DAQmx Start Task.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
					<Item Name="DAQmx Start Trigger (Digital Edge).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Digital Edge).vi"/>
					<Item Name="DAQmx Stop Task.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
					<Item Name="DAQmx Timing (Implicit).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/timing.llb/DAQmx Timing (Implicit).vi"/>
					<Item Name="DAQmx Timing (Sample Clock).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/timing.llb/DAQmx Timing (Sample Clock).vi"/>
					<Item Name="DAQmx Wait Until Done.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/configure/task.llb/DAQmx Wait Until Done.vi"/>
					<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
					<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
					<Item Name="Details Display Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
					<Item Name="DialogType.ctl" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
					<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
					<Item Name="Error Code Database.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
					<Item Name="ErrWarn.ctl" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
					<Item Name="eventvkey.ctl" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
					<Item Name="Find Tag.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
					<Item Name="Format Message String.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
					<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
					<Item Name="General Error Handler.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
					<Item Name="Get Full Terminal Name.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/Utility/Get Full Terminal Name.vi"/>
					<Item Name="Get String Text Bounds.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
					<Item Name="Get Text Rect.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
					<Item Name="GetHelpDir.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
					<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
					<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
					<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
					<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
					<Item Name="Not Found Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
					<Item Name="Pulse.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/Utility/Pulse.vi"/>
					<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
					<Item Name="Set Bold Text.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
					<Item Name="Set String Value.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
					<Item Name="subTimeDelay.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
					<Item Name="TagReturnType.ctl" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
					<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
					<Item Name="Three Button Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
			</Item>
			<Item Name="DCPower" Type="Folder">
				<Item Name="DCPowerInterface" Type="Folder">
					<Item Name="API" Type="Folder">
						<Item Name="DC Current" Type="Folder"/>
						<Item Name="DC Voltage" Type="Folder"/>
						<Item Name="Pulse Current" Type="Folder"/>
						<Item Name="Pulse Voltage" Type="Folder"/>
						<Item Name="Scope Mode" Type="Folder"/>
					</Item>
					<Item Name="API Demo" Type="Folder"/>
					<Item Name="Ctrl" Type="Folder"/>
					<Item Name="Private SubVI" Type="Folder"/>
					<Item Name="DCPower.aliases" Type="Document" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.aliases"/>
					<Item Name="DCPower.lvlib" Type="Library" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlib"/>
					<Item Name="DCPower.lvlibp" Type="LVLibp" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp">
						<Item Name="BuildHelpPath.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
						<Item Name="Check Special Tags.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
						<Item Name="DAQmx Connect Terminals.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/DAQmx/configure/routing.llb/DAQmx Connect Terminals.vi"/>
						<Item Name="DAQmx Disconnect Terminals.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/DAQmx/configure/routing.llb/DAQmx Disconnect Terminals.vi"/>
						<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
						<Item Name="DCPower_Interface.lvclass" Type="LVClass" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/DCPower_Interface.lvclass"/>
						<Item Name="Details Display Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
						<Item Name="DialogType.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
						<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
						<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
						<Item Name="Error Code Database.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
						<Item Name="ErrWarn.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
						<Item Name="eventvkey.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
						<Item Name="Find Tag.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
						<Item Name="Format Message String.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
						<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
						<Item Name="General Error Handler.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
						<Item Name="Get String Text Bounds.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
						<Item Name="Get Text Rect.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
						<Item Name="GetHelpDir.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
						<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
						<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
						<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
						<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
						<Item Name="NI_AALBase.lvlib" Type="Library" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Analysis/NI_AALBase.lvlib"/>
						<Item Name="niDCPower Abort.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Abort.vi"/>
						<Item Name="niDCPower Aperture Time Units.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Aperture Time Units.ctl"/>
						<Item Name="niDCPower Auto Zero.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Auto Zero.ctl"/>
						<Item Name="niDCPower Close.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Close.vi"/>
						<Item Name="niDCPower Commit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Commit.vi"/>
						<Item Name="niDCPower Configure Aperture Time.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Aperture Time.vi"/>
						<Item Name="niDCPower Configure Auto Zero.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Auto Zero.vi"/>
						<Item Name="niDCPower Configure Current Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Current Level Range.vi"/>
						<Item Name="niDCPower Configure Current Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Current Level.vi"/>
						<Item Name="niDCPower Configure Current Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Current Limit Range.vi"/>
						<Item Name="niDCPower Configure Current Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Current Limit.vi"/>
						<Item Name="niDCPower Configure Digital Edge Measure Trigger.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Digital Edge Measure Trigger.vi"/>
						<Item Name="niDCPower Configure Digital Edge Source Trigger.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Digital Edge Source Trigger.vi"/>
						<Item Name="niDCPower Configure Digital Edge Start Trigger.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Digital Edge Start Trigger.vi"/>
						<Item Name="niDCPower Configure Output Enabled.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Output Enabled.vi"/>
						<Item Name="niDCPower Configure Output Function.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Output Function.vi"/>
						<Item Name="niDCPower Configure Output Resistance.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Output Resistance.vi"/>
						<Item Name="niDCPower Configure Power Line Frequency.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Power Line Frequency.vi"/>
						<Item Name="niDCPower Configure Pulse Bias Current Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Current Level.vi"/>
						<Item Name="niDCPower Configure Pulse Bias Current Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Current Limit.vi"/>
						<Item Name="niDCPower Configure Pulse Bias Voltage Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Voltage Level.vi"/>
						<Item Name="niDCPower Configure Pulse Bias Voltage Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Voltage Limit.vi"/>
						<Item Name="niDCPower Configure Pulse Current Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Level Range.vi"/>
						<Item Name="niDCPower Configure Pulse Current Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Level.vi"/>
						<Item Name="niDCPower Configure Pulse Current Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Limit Range.vi"/>
						<Item Name="niDCPower Configure Pulse Current Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Limit.vi"/>
						<Item Name="niDCPower Configure Pulse Voltage Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Level Range.vi"/>
						<Item Name="niDCPower Configure Pulse Voltage Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Level.vi"/>
						<Item Name="niDCPower Configure Pulse Voltage Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Limit Range.vi"/>
						<Item Name="niDCPower Configure Pulse Voltage Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Limit.vi"/>
						<Item Name="niDCPower Configure Source Mode.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Source Mode.vi"/>
						<Item Name="niDCPower Configure Voltage Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level Range.vi"/>
						<Item Name="niDCPower Configure Voltage Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level.vi"/>
						<Item Name="niDCPower Configure Voltage Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Voltage Limit Range.vi"/>
						<Item Name="niDCPower Configure Voltage Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Voltage Limit.vi"/>
						<Item Name="niDCPower Current Limit Behavior.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Current Limit Behavior.ctl"/>
						<Item Name="niDCPower Digital Edge - Edge.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Digital Edge - Edge.ctl"/>
						<Item Name="niDCPower Disable Source Trigger.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Disable Source Trigger.vi"/>
						<Item Name="niDCPower Disable Start Trigger.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Disable Start Trigger.vi"/>
						<Item Name="niDCPower Export Signal - Signal.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Export Signal - Signal.ctl"/>
						<Item Name="niDCPower Export Signal.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Export Signal.vi"/>
						<Item Name="niDCPower Fetch Multiple.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Fetch Multiple.vi"/>
						<Item Name="niDCPower Initialize With Channels.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Initialize With Channels.vi"/>
						<Item Name="niDCPower Initiate.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Initiate.vi"/>
						<Item Name="niDCPower IVI Error Converter.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower IVI Error Converter.vi"/>
						<Item Name="niDCPower Measure Multiple.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Measure Multiple.vi"/>
						<Item Name="niDCPower Output Function.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Output Function.ctl"/>
						<Item Name="niDCPower Power Line Frequency.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Power Line Frequency.ctl"/>
						<Item Name="niDCPower Reset Device.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Reset Device.vi"/>
						<Item Name="niDCPower Reset.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Reset.vi"/>
						<Item Name="niDCPower Set Sequence.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Set Sequence.vi"/>
						<Item Name="niDCPower Source Mode.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Source Mode.ctl"/>
						<Item Name="niDCPower Wait For Event - Event.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Wait For Event - Event.ctl"/>
						<Item Name="niDCPower Wait For Event.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Wait For Event.vi"/>
						<Item Name="niModInst Close Installed Devices Session.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Close Installed Devices Session.vi"/>
						<Item Name="niModInst Get Installed Device Attribute (String).vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Get Installed Device Attribute (String).vi"/>
						<Item Name="niModInst Open Installed Devices Session.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Open Installed Devices Session.vi"/>
						<Item Name="niModInst Set Error.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Set Error.vi"/>
						<Item Name="Not Found Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
						<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
						<Item Name="Set Bold Text.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
						<Item Name="Set String Value.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
						<Item Name="subTimeDelay.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
						<Item Name="TagReturnType.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
						<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
						<Item Name="Three Button Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
						<Item Name="Trim Whitespace.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
						<Item Name="whitespace.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					</Item>
					<Item Name="DCPower.lvlps" Type="Document" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvlps"/>
					<Item Name="DCPower.lvproj" Type="Document" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower.lvproj"/>
					<Item Name="DCPower_Interface.lvlibp" Type="LVLibp" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp">
						<Item Name="BuildHelpPath.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
						<Item Name="Check Special Tags.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
						<Item Name="DCPower_Interface.lvclass" Type="LVClass" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/DCPower_Interface.lvclass"/>
						<Item Name="Details Display Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
						<Item Name="DialogType.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
						<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
						<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
						<Item Name="Error Code Database.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
						<Item Name="ErrWarn.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
						<Item Name="eventvkey.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
						<Item Name="Find Tag.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
						<Item Name="Format Message String.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
						<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
						<Item Name="General Error Handler.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
						<Item Name="Get String Text Bounds.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
						<Item Name="Get Text Rect.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
						<Item Name="GetHelpDir.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
						<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
						<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
						<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
						<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
						<Item Name="niDCPower Abort.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Abort.vi"/>
						<Item Name="niDCPower Aperture Time Units.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Aperture Time Units.ctl"/>
						<Item Name="niDCPower Close.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Close.vi"/>
						<Item Name="niDCPower Configure Aperture Time.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Aperture Time.vi"/>
						<Item Name="niDCPower Configure Current Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Current Level Range.vi"/>
						<Item Name="niDCPower Configure Current Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Current Level.vi"/>
						<Item Name="niDCPower Configure Current Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Current Limit Range.vi"/>
						<Item Name="niDCPower Configure Current Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Current Limit.vi"/>
						<Item Name="niDCPower Configure Output Enabled.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Output Enabled.vi"/>
						<Item Name="niDCPower Configure Output Function.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Output Function.vi"/>
						<Item Name="niDCPower Configure Output Resistance.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Output Resistance.vi"/>
						<Item Name="niDCPower Configure Pulse Bias Current Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Current Level.vi"/>
						<Item Name="niDCPower Configure Pulse Bias Current Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Current Limit.vi"/>
						<Item Name="niDCPower Configure Pulse Bias Voltage Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Voltage Level.vi"/>
						<Item Name="niDCPower Configure Pulse Bias Voltage Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Voltage Limit.vi"/>
						<Item Name="niDCPower Configure Pulse Current Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Level Range.vi"/>
						<Item Name="niDCPower Configure Pulse Current Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Level.vi"/>
						<Item Name="niDCPower Configure Pulse Current Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Limit Range.vi"/>
						<Item Name="niDCPower Configure Pulse Current Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Limit.vi"/>
						<Item Name="niDCPower Configure Pulse Voltage Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Level Range.vi"/>
						<Item Name="niDCPower Configure Pulse Voltage Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Level.vi"/>
						<Item Name="niDCPower Configure Pulse Voltage Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Limit Range.vi"/>
						<Item Name="niDCPower Configure Pulse Voltage Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Limit.vi"/>
						<Item Name="niDCPower Configure Source Mode.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Source Mode.vi"/>
						<Item Name="niDCPower Configure Voltage Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level Range.vi"/>
						<Item Name="niDCPower Configure Voltage Level.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level.vi"/>
						<Item Name="niDCPower Configure Voltage Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Voltage Limit Range.vi"/>
						<Item Name="niDCPower Configure Voltage Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Voltage Limit.vi"/>
						<Item Name="niDCPower Current Limit Behavior.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Current Limit Behavior.ctl"/>
						<Item Name="niDCPower Initialize With Channels.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Initialize With Channels.vi"/>
						<Item Name="niDCPower Initiate.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Initiate.vi"/>
						<Item Name="niDCPower IVI Error Converter.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower IVI Error Converter.vi"/>
						<Item Name="niDCPower Measure Multiple.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Measure Multiple.vi"/>
						<Item Name="niDCPower Output Function.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Output Function.ctl"/>
						<Item Name="niDCPower Source Mode.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Source Mode.ctl"/>
						<Item Name="niDCPower Wait For Event - Event.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Wait For Event - Event.ctl"/>
						<Item Name="niDCPower Wait For Event.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Wait For Event.vi"/>
						<Item Name="niModInst Close Installed Devices Session.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Close Installed Devices Session.vi"/>
						<Item Name="niModInst Get Installed Device Attribute (String).vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Get Installed Device Attribute (String).vi"/>
						<Item Name="niModInst Open Installed Devices Session.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Open Installed Devices Session.vi"/>
						<Item Name="niModInst Set Error.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Set Error.vi"/>
						<Item Name="Not Found Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
						<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
						<Item Name="Set Bold Text.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
						<Item Name="Set String Value.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
						<Item Name="TagReturnType.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
						<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
						<Item Name="Three Button Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
						<Item Name="Trim Whitespace.vi" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
						<Item Name="whitespace.ctl" Type="VI" URL="../Hardware Abstraction Layer/DCPower/DCPowerInterface/DCPower_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					</Item>
				</Item>
			</Item>
			<Item Name="Scope" Type="Folder">
				<Item Name="API" Type="Folder"/>
				<Item Name="CTRL" Type="Folder"/>
				<Item Name="Scope.lvlib" Type="Library" URL="../Hardware Abstraction Layer/Scope/Scope.lvlib"/>
				<Item Name="Scope.lvlibp" Type="LVLibp" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp">
					<Item Name="BuildHelpPath.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
					<Item Name="Check Special Tags.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
					<Item Name="Details Display Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
					<Item Name="DialogType.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
					<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Error Code Database.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
					<Item Name="ErrWarn.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
					<Item Name="eventvkey.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
					<Item Name="Find Tag.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
					<Item Name="Format Message String.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
					<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
					<Item Name="General Error Handler.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
					<Item Name="Get String Text Bounds.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
					<Item Name="Get Text Rect.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
					<Item Name="GetHelpDir.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
					<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
					<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
					<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
					<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
					<Item Name="niDCPower Abort.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Abort.vi"/>
					<Item Name="niDCPower Aperture Time Units.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Aperture Time Units.ctl"/>
					<Item Name="niDCPower Close.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Close.vi"/>
					<Item Name="niDCPower Configure Aperture Time.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Aperture Time.vi"/>
					<Item Name="niDCPower Configure Output Function.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Output Function.vi"/>
					<Item Name="niDCPower Configure Pulse Bias Current Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Current Limit.vi"/>
					<Item Name="niDCPower Configure Pulse Bias Voltage Level.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Voltage Level.vi"/>
					<Item Name="niDCPower Configure Pulse Current Limit Range.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Limit Range.vi"/>
					<Item Name="niDCPower Configure Pulse Current Limit.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Limit.vi"/>
					<Item Name="niDCPower Configure Pulse Voltage Level Range.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Level Range.vi"/>
					<Item Name="niDCPower Configure Pulse Voltage Level.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Level.vi"/>
					<Item Name="niDCPower Configure Source Mode.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Configure Source Mode.vi"/>
					<Item Name="niDCPower Export Signal - Signal.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Export Signal - Signal.ctl"/>
					<Item Name="niDCPower Export Signal.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Export Signal.vi"/>
					<Item Name="niDCPower Initialize With Channels.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Initialize With Channels.vi"/>
					<Item Name="niDCPower Initiate.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Initiate.vi"/>
					<Item Name="niDCPower IVI Error Converter.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower IVI Error Converter.vi"/>
					<Item Name="niDCPower Output Function.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Output Function.ctl"/>
					<Item Name="niDCPower Reset.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Reset.vi"/>
					<Item Name="niDCPower Source Mode.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niDCPower/nidcpower.llb/niDCPower Source Mode.ctl"/>
					<Item Name="niModInst Close Installed Devices Session.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Close Installed Devices Session.vi"/>
					<Item Name="niModInst Get Installed Device Attribute (String).vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Get Installed Device Attribute (String).vi"/>
					<Item Name="niModInst Open Installed Devices Session.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Open Installed Devices Session.vi"/>
					<Item Name="niModInst Set Error.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Set Error.vi"/>
					<Item Name="niScope Abort.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Acquire/Fetch/niScope Abort.vi"/>
					<Item Name="niScope Acquisition Status.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Acquire/Fetch/niScope Acquisition Status.vi"/>
					<Item Name="niScope acquisition type.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope acquisition type.ctl"/>
					<Item Name="niScope Actual Num Wfms.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Utility/niScope Actual Num Wfms.vi"/>
					<Item Name="niScope Actual Record Length.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/Horizontal/niScope Actual Record Length.vi"/>
					<Item Name="niScope Close.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/niScope Close.vi"/>
					<Item Name="niScope Commit.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Utility/niScope Commit.vi"/>
					<Item Name="niScope Configure Acquisition.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/niScope Configure Acquisition.vi"/>
					<Item Name="niScope Configure Chan Characteristics.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/Vertical/niScope Configure Chan Characteristics.vi"/>
					<Item Name="niScope Configure Horizontal Timing.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/Horizontal/niScope Configure Horizontal Timing.vi"/>
					<Item Name="niScope Configure Trigger Digital.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/Trigger/niScope Configure Trigger Digital.vi"/>
					<Item Name="niScope Configure Trigger Edge.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/Trigger/niScope Configure Trigger Edge.vi"/>
					<Item Name="niScope Configure Trigger Immediate.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/Trigger/niScope Configure Trigger Immediate.vi"/>
					<Item Name="niScope Configure Vertical.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/Vertical/niScope Configure Vertical.vi"/>
					<Item Name="niScope export destinations.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope export destinations.ctl"/>
					<Item Name="niScope Export Signal.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Configure/Trigger/niScope Export Signal.vi"/>
					<Item Name="niScope exportable signals.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope exportable signals.ctl"/>
					<Item Name="niScope Fetch Error Chain.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Acquire/Fetch/niScope Fetch Error Chain.vi"/>
					<Item Name="niScope Get Session Reference.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Utility/niScope Get Session Reference.vi"/>
					<Item Name="niScope Initialize.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/niScope Initialize.vi"/>
					<Item Name="niScope Initiate Acquisition.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Acquire/Fetch/niScope Initiate Acquisition.vi"/>
					<Item Name="niScope Is Device Ready.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Utility/niScope Is Device Ready.vi"/>
					<Item Name="niScope LabVIEW Error.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Utility/niScope LabVIEW Error.vi"/>
					<Item Name="niScope Multi Fetch WDT.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Acquire/Fetch/niScope Multi Fetch WDT.vi"/>
					<Item Name="niScope Multi Read WDT.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Acquire/niScope Multi Read WDT.vi"/>
					<Item Name="niScope timestamp type.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope timestamp type.ctl"/>
					<Item Name="niScope trigger coupling.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope trigger coupling.ctl"/>
					<Item Name="niScope trigger slope.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope trigger slope.ctl"/>
					<Item Name="niScope trigger source digital.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope trigger source digital.ctl"/>
					<Item Name="niScope trigger source.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope trigger source.ctl"/>
					<Item Name="niScope trigger window mode.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope trigger window mode.ctl"/>
					<Item Name="niScope vertical coupling.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope vertical coupling.ctl"/>
					<Item Name="niScope which signal.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/instr.lib/niScope/Controls/niScope which signal.ctl"/>
					<Item Name="Not Found Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
					<Item Name="Scope_Interface.lvclass" Type="LVClass" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/Scope_Interface.lvclass"/>
					<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
					<Item Name="Set Bold Text.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
					<Item Name="Set String Value.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
					<Item Name="Simple Error Handler.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Simple Error Handler.vi"/>
					<Item Name="subTimeDelay.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
					<Item Name="TagReturnType.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
					<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
					<Item Name="Three Button Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../Hardware Abstraction Layer/Scope/Scope.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
				<Item Name="Scope_Interface.aliases" Type="Document" URL="../Hardware Abstraction Layer/Scope/Scope_Interface.aliases"/>
				<Item Name="Scope_Interface.lvlps" Type="Document" URL="../Hardware Abstraction Layer/Scope/Scope_Interface.lvlps"/>
				<Item Name="Scope_Interface.lvproj" Type="Document" URL="../Hardware Abstraction Layer/Scope/Scope_Interface.lvproj"/>
			</Item>
			<Item Name="Switch" Type="Folder">
				<Item Name="Access to elements" Type="Folder"/>
				<Item Name="API" Type="Folder"/>
				<Item Name="Demo" Type="Folder"/>
				<Item Name="Private" Type="Folder"/>
				<Item Name="Switch_Interface.aliases" Type="Document" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.aliases"/>
				<Item Name="Switch_Interface.lvlib" Type="Library" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlib"/>
				<Item Name="Switch_Interface.lvlibp" Type="LVLibp" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp">
					<Item Name="BuildHelpPath.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/BuildHelpPath.vi"/>
					<Item Name="Check Special Tags.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Check Special Tags.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Convert property node font to graphics font.vi"/>
					<Item Name="Details Display Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Details Display Dialog.vi"/>
					<Item Name="DialogType.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogType.ctl"/>
					<Item Name="DialogTypeEnum.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/DialogTypeEnum.ctl"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Error Code Database.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
					<Item Name="ErrWarn.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/ErrWarn.ctl"/>
					<Item Name="eventvkey.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/event_ctls.llb/eventvkey.ctl"/>
					<Item Name="Find Tag.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Find Tag.vi"/>
					<Item Name="Format Message String.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Format Message String.vi"/>
					<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler Core CORE.vi"/>
					<Item Name="General Error Handler.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/General Error Handler.vi"/>
					<Item Name="Get String Text Bounds.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Get String Text Bounds.vi"/>
					<Item Name="Get Text Rect.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Get Text Rect.vi"/>
					<Item Name="GetHelpDir.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetHelpDir.vi"/>
					<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/GetRTHostConnectedProp.vi"/>
					<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Longest Line Length in Pixels.vi"/>
					<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
					<Item Name="LVRectTypeDef.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
					<Item Name="niModInst Close Installed Devices Session.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Close Installed Devices Session.vi"/>
					<Item Name="niModInst Get Installed Device Attribute (String).vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Get Installed Device Attribute (String).vi"/>
					<Item Name="niModInst Open Installed Devices Session.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Open Installed Devices Session.vi"/>
					<Item Name="niModInst Set Error.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niModInst/niModInst Set Error.vi"/>
					<Item Name="niSwitch Close.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niSwitch/niSwitch.LLB/niSwitch Close.vi"/>
					<Item Name="niSwitch Initialize With Topology.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niSwitch/niSwitch.LLB/niSwitch Initialize With Topology.vi"/>
					<Item Name="niSwitch IVI Error Converter.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niSwitch/niSwitch.LLB/niSwitch IVI Error Converter.vi"/>
					<Item Name="niSwitch Relay Action.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niSwitch/niSwitch.LLB/niSwitch Relay Action.ctl"/>
					<Item Name="niSwitch Relay Control.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niSwitch/niSwitch.LLB/niSwitch Relay Control.vi"/>
					<Item Name="niSwitch Topologies.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/instr.lib/niSwitch/niSwitch.LLB/niSwitch Topologies.ctl"/>
					<Item Name="Not Found Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Not Found Dialog.vi"/>
					<Item Name="Search and Replace Pattern.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Search and Replace Pattern.vi"/>
					<Item Name="Set Bold Text.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set Bold Text.vi"/>
					<Item Name="Set String Value.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Set String Value.vi"/>
					<Item Name="Switch_Interface.lvclass" Type="LVClass" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/Switch_Interface.lvclass"/>
					<Item Name="TagReturnType.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/TagReturnType.ctl"/>
					<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog CORE.vi"/>
					<Item Name="Three Button Dialog.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Three Button Dialog.vi"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
				<Item Name="Switch_Interface.lvlps" Type="Document" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvlps"/>
				<Item Name="Switch_Interface.lvproj" Type="Document" URL="../Hardware Abstraction Layer/Switch/Switch_Interface.lvproj"/>
			</Item>
		</Item>
		<Item Name="Procedure" Type="Folder" URL="../Procedure">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Utility" Type="Folder" URL="../Utility">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="UCC2894" Type="Folder">
			<Item Name="Development Environment" Type="Folder">
				<Item Name="Test names.ctl" Type="VI" URL="../UCC2894/Development Environment/Test names.ctl"/>
				<Item Name="Test Panel GD.ctl" Type="VI" URL="../UCC2894/Development Environment/Test Panel GD.ctl"/>
				<Item Name="Test Panel.vi" Type="VI" URL="../UCC2894/Development Environment/Test Panel.vi"/>
			</Item>
			<Item Name="Tests" Type="Folder">
				<Item Name="Test_01" Type="Folder">
					<Item Name="Test_01.lvlib" Type="Library" URL="../UCC2894/Tests/Test_01/Test_01.lvlib"/>
				</Item>
				<Item Name="Test_02" Type="Folder">
					<Item Name="Test_02.lvlib" Type="Library" URL="../UCC2894/Tests/Test_02/Test_02.lvlib"/>
				</Item>
				<Item Name="Test_03" Type="Folder">
					<Item Name="Test_03.lvlib" Type="Library" URL="../UCC2894/Tests/Test_03/Test_03.lvlib"/>
				</Item>
				<Item Name="Test_04" Type="Folder">
					<Item Name="Test_04.lvlib" Type="Library" URL="../UCC2894/Tests/Test_04/Test_04.lvlib"/>
				</Item>
				<Item Name="Test_05" Type="Folder">
					<Item Name="Test_05.lvlib" Type="Library" URL="../UCC2894/Tests/Test_05/Test_05.lvlib"/>
				</Item>
				<Item Name="Test_06" Type="Folder">
					<Item Name="Test_06.lvlib" Type="Library" URL="../UCC2894/Tests/Test_06/Test_06.lvlib"/>
				</Item>
				<Item Name="Test_07" Type="Folder">
					<Item Name="Test_07.lvlib" Type="Library" URL="../UCC2894/Tests/Test_07/Test_07.lvlib"/>
				</Item>
				<Item Name="Test_08" Type="Folder">
					<Item Name="Test_08.lvlib" Type="Library" URL="../UCC2894/Tests/Test_08/Test_08.lvlib"/>
				</Item>
				<Item Name="Test_09" Type="Folder">
					<Item Name="Test_09.lvlib" Type="Library" URL="../UCC2894/Tests/Test_09/Test_09.lvlib"/>
				</Item>
				<Item Name="Test_10" Type="Folder">
					<Item Name="Test_10.lvlib" Type="Library" URL="../UCC2894/Tests/Test_10/Test_10.lvlib"/>
				</Item>
				<Item Name="Test_11" Type="Folder">
					<Item Name="Test_11.lvlib" Type="Library" URL="../UCC2894/Tests/Test_11/Test_11.lvlib"/>
				</Item>
				<Item Name="Test_12" Type="Folder">
					<Item Name="Test_12.lvlib" Type="Library" URL="../UCC2894/Tests/Test_12/Test_12.lvlib"/>
				</Item>
				<Item Name="Test_13" Type="Folder">
					<Item Name="Test_13.lvlib" Type="Library" URL="../UCC2894/Tests/Test_13/Test_13.lvlib"/>
				</Item>
				<Item Name="Test_14" Type="Folder">
					<Item Name="Test_14.lvlib" Type="Library" URL="../UCC2894/Tests/Test_14/Test_14.lvlib"/>
				</Item>
				<Item Name="Test_15" Type="Folder">
					<Item Name="Test_15.lvlib" Type="Library" URL="../UCC2894/Tests/Test_15/Test_15.lvlib"/>
				</Item>
				<Item Name="Test_16" Type="Folder">
					<Item Name="Test_16.lvlib" Type="Library" URL="../UCC2894/Tests/Test_16/Test_16.lvlib"/>
				</Item>
				<Item Name="Test_17" Type="Folder">
					<Item Name="Test_17.lvlib" Type="Library" URL="../UCC2894/Tests/Test_17/Test_17.lvlib"/>
				</Item>
				<Item Name="Test_18" Type="Folder">
					<Item Name="Test_18.lvlib" Type="Library" URL="../UCC2894/Tests/Test_18/Test_18.lvlib"/>
				</Item>
				<Item Name="Test_19" Type="Folder">
					<Item Name="Test_19.lvlib" Type="Library" URL="../UCC2894/Tests/Test_19/Test_19.lvlib"/>
				</Item>
				<Item Name="Test_20" Type="Folder">
					<Item Name="Test_20.lvlib" Type="Library" URL="../UCC2894/Tests/Test_20/Test_20.lvlib"/>
				</Item>
				<Item Name="Test_21" Type="Folder">
					<Item Name="Test_21.lvlib" Type="Library" URL="../UCC2894/Tests/Test_21/Test_21.lvlib"/>
				</Item>
				<Item Name="Test_22" Type="Folder">
					<Item Name="Test_22.lvlib" Type="Library" URL="../UCC2894/Tests/Test_22/Test_22.lvlib"/>
				</Item>
				<Item Name="Test_23" Type="Folder">
					<Item Name="Test_23.lvlib" Type="Library" URL="../UCC2894/Tests/Test_23/Test_23.lvlib"/>
				</Item>
				<Item Name="Test_24" Type="Folder">
					<Item Name="Test_24.lvlib" Type="Library" URL="../UCC2894/Tests/Test_24/Test_24.lvlib"/>
				</Item>
				<Item Name="Test_25" Type="Folder">
					<Item Name="Test_25.lvlib" Type="Library" URL="../UCC2894/Tests/Test_25/Test_25.lvlib"/>
				</Item>
				<Item Name="Test_26" Type="Folder">
					<Item Name="Test_26.lvlib" Type="Library" URL="../UCC2894/Tests/Test_26/Test_26.lvlib"/>
				</Item>
				<Item Name="Test_Interface.lvlibp" Type="LVLibp" URL="../UCC2894/Tests/Test_Interface.lvlibp">
					<Item Name="Test_Interface.lvclass" Type="LVClass" URL="../UCC2894/Tests/Test_Interface.lvlibp/Test_Interface.lvclass"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Timing.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing.vi"/>
				<Item Name="DAQmx Timing (Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Handshaking).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Handshaking).vi"/>
				<Item Name="DAQmx Timing (Implicit).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Implicit).vi"/>
				<Item Name="DAQmx Timing (Use Waveform).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Use Waveform).vi"/>
				<Item Name="DAQmx Timing (Change Detection).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Change Detection).vi"/>
				<Item Name="DAQmx Timing (Burst Import Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Import Clock).vi"/>
				<Item Name="DAQmx Timing (Burst Export Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Export Clock).vi"/>
				<Item Name="DAQmx Timing (Pipelined Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Pipelined Sample Clock).vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Control Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Control Task.vi"/>
				<Item Name="DAQmx Connect Terminals.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/routing.llb/DAQmx Connect Terminals.vi"/>
				<Item Name="DAQmx Disconnect Terminals.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/routing.llb/DAQmx Disconnect Terminals.vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="DAQmx Is Task Done.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Is Task Done.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="nisyscfg.lvlib" Type="Library" URL="/&lt;vilib&gt;/nisyscfg/nisyscfg.lvlib"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="Waveform Min Max.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Waveform Min Max.vi"/>
				<Item Name="NI_MAPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MAPro.lvlib"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="lveventtype.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/lveventtype.ctl"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Check for multiple of dt.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for multiple of dt.vi"/>
				<Item Name="WDT Number of Waveform Samples SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples SGL.vi"/>
				<Item Name="WDT Number of Waveform Samples I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I8.vi"/>
				<Item Name="WDT Number of Waveform Samples I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I32.vi"/>
				<Item Name="WDT Number of Waveform Samples I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples I16.vi"/>
				<Item Name="WDT Number of Waveform Samples EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples EXT.vi"/>
				<Item Name="WDT Number of Waveform Samples CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples CDB.vi"/>
				<Item Name="WDT Number of Waveform Samples DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Number of Waveform Samples DBL.vi"/>
				<Item Name="Number of Waveform Samples.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Number of Waveform Samples.vi"/>
				<Item Name="WDT Get Waveform Subset SGL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset SGL.vi"/>
				<Item Name="WDT Get Waveform Subset I8.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I8.vi"/>
				<Item Name="WDT Get Waveform Subset I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I32.vi"/>
				<Item Name="WDT Get Waveform Subset I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset I16.vi"/>
				<Item Name="WDT Get Waveform Subset EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset EXT.vi"/>
				<Item Name="WDT Get Waveform Subset CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset CDB.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="DTbl Digital Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Subset.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DWDT Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Get Waveform Subset.vi"/>
				<Item Name="WDT Get Waveform Subset DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Get Waveform Subset DBL.vi"/>
				<Item Name="Get Waveform Subset.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Get Waveform Subset.vi"/>
				<Item Name="DU64_U32SubtractWithBorrow.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/TSOps.llb/DU64_U32SubtractWithBorrow.vi"/>
				<Item Name="I128 Timestamp.ctl" Type="VI" URL="/&lt;vilib&gt;/Waveform/TSOps.llb/I128 Timestamp.ctl"/>
				<Item Name="Timestamp Subtract.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/TSOps.llb/Timestamp Subtract.vi"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Trigger.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Trigger.vi"/>
				<Item Name="DAQmx Start Trigger (None).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (None).vi"/>
				<Item Name="DAQmx Start Trigger (Digital Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Digital Edge).vi"/>
				<Item Name="DAQmx Start Trigger (Analog Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Analog Edge).vi"/>
				<Item Name="DAQmx Reference Trigger (Analog Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Analog Edge).vi"/>
				<Item Name="DAQmx Advance Trigger (Digital Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Advance Trigger (Digital Edge).vi"/>
				<Item Name="DAQmx Reference Trigger (None).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (None).vi"/>
				<Item Name="DAQmx Reference Trigger (Digital Edge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Digital Edge).vi"/>
				<Item Name="DAQmx Start Trigger (Analog Window).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Analog Window).vi"/>
				<Item Name="DAQmx Reference Trigger (Analog Window).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Analog Window).vi"/>
				<Item Name="DAQmx Advance Trigger (None).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Advance Trigger (None).vi"/>
				<Item Name="DAQmx Reference Trigger (Digital Pattern).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Reference Trigger (Digital Pattern).vi"/>
				<Item Name="DAQmx Start Trigger (Digital Pattern).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/trigger.llb/DAQmx Start Trigger (Digital Pattern).vi"/>
				<Item Name="DAQmx Wait Until Done.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Wait Until Done.vi"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="Index Waveform Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Index Waveform Array.vi"/>
				<Item Name="WDT Index Channel by Name CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel by Name CDB.vi"/>
				<Item Name="WDT Index Channel CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel CDB.vi"/>
				<Item Name="DWDT Index Channel.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Index Channel.vi"/>
				<Item Name="DWDT Index Channel by Name.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Index Channel by Name.vi"/>
				<Item Name="WDT Index Channel by Name I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel by Name I16.vi"/>
				<Item Name="WDT Index Channel by Name I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel by Name I32.vi"/>
				<Item Name="WDT Index Channel I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel I16.vi"/>
				<Item Name="WDT Index Channel I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel I32.vi"/>
				<Item Name="WDT Index Channel DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel DBL.vi"/>
				<Item Name="WDT Index Channel by Name DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel by Name DBL.vi"/>
				<Item Name="WDT Index Channel by Name I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel by Name I64.vi"/>
				<Item Name="WDT Index Channel I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Index Channel I64.vi"/>
			</Item>
			<Item Name="instr.lib" Type="Folder">
				<Item Name="niDCPower Configure Voltage Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level.vi"/>
				<Item Name="niDCPower Configure Current Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Current Limit.vi"/>
				<Item Name="niDCPower Configure Voltage Level Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level Range.vi"/>
				<Item Name="niDCPower Configure Current Limit Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Current Limit Range.vi"/>
				<Item Name="niScope trigger slope.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl"/>
				<Item Name="niScope trigger window mode.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl"/>
				<Item Name="niScope trigger coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl"/>
				<Item Name="niScope vertical coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl"/>
				<Item Name="niSwitch Topologies.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Topologies.ctl"/>
				<Item Name="niDCPower Measure Multiple.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Measure Multiple.vi"/>
				<Item Name="niDCPower Fetch Multiple.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Fetch Multiple.vi"/>
				<Item Name="niDCPower Export Signal - Signal.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Export Signal - Signal.ctl"/>
				<Item Name="niDCPower Output Function.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Output Function.ctl"/>
				<Item Name="niDCPower Source Mode.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Source Mode.ctl"/>
				<Item Name="niDCPower Aperture Time Units.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Aperture Time Units.ctl"/>
				<Item Name="niDCPower Abort.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Abort.vi"/>
				<Item Name="niDCPower Configure Source Mode.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Source Mode.vi"/>
				<Item Name="niDCPower Configure Output Function.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Output Function.vi"/>
				<Item Name="niDCPower Configure Current Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Current Level.vi"/>
				<Item Name="niDCPower Configure Voltage Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Voltage Limit.vi"/>
				<Item Name="niDCPower Configure Current Level Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Current Level Range.vi"/>
				<Item Name="niDCPower Configure Voltage Limit Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Voltage Limit Range.vi"/>
				<Item Name="niDCPower Configure Output Resistance.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Output Resistance.vi"/>
				<Item Name="niDCPower Configure Trigger (Poly).vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Trigger (Poly).vi"/>
				<Item Name="niDCPower Configure Digital Edge Source Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Digital Edge Source Trigger.vi"/>
				<Item Name="niDCPower Configure Digital Edge Start Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Digital Edge Start Trigger.vi"/>
				<Item Name="niDCPower Disable Start Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Disable Start Trigger.vi"/>
				<Item Name="niDCPower Disable Source Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Disable Source Trigger.vi"/>
				<Item Name="niDCPower Configure Digital Edge Measure Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Digital Edge Measure Trigger.vi"/>
				<Item Name="niDCPower Configure Aperture Time.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Aperture Time.vi"/>
				<Item Name="niDCPower Export Signal.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Export Signal.vi"/>
				<Item Name="niDCPower Initiate.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Initiate.vi"/>
				<Item Name="niDCPower Commit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Commit.vi"/>
				<Item Name="niDCPower Configure Pulse Current Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Level.vi"/>
				<Item Name="niDCPower Set Sequence.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Set Sequence.vi"/>
				<Item Name="niDCPower Configure Pulse Current Level Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Level Range.vi"/>
				<Item Name="niDCPower Configure Pulse Bias Current Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Current Level.vi"/>
				<Item Name="niDCPower Configure Pulse Voltage Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Limit.vi"/>
				<Item Name="niDCPower Configure Pulse Voltage Limit Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Limit Range.vi"/>
				<Item Name="niDCPower Configure Pulse Bias Voltage Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Voltage Limit.vi"/>
				<Item Name="niDCPower Wait For Event - Event.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Wait For Event - Event.ctl"/>
				<Item Name="niDCPower Configure Pulse Voltage Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Level.vi"/>
				<Item Name="niDCPower Configure Pulse Voltage Level Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Voltage Level Range.vi"/>
				<Item Name="niDCPower Configure Pulse Bias Voltage Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Voltage Level.vi"/>
				<Item Name="niDCPower Configure Pulse Current Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Limit.vi"/>
				<Item Name="niDCPower Configure Pulse Current Limit Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Current Limit Range.vi"/>
				<Item Name="niDCPower Configure Pulse Bias Current Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Pulse Bias Current Limit.vi"/>
				<Item Name="niDCPower Wait For Event.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Wait For Event.vi"/>
				<Item Name="niDCPower Configure Output Enabled.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Output Enabled.vi"/>
				<Item Name="niDCPower Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Close.vi"/>
				<Item Name="niModInst Open Installed Devices Session.vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Open Installed Devices Session.vi"/>
				<Item Name="niModInst Get Installed Device Attribute (poly).vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Get Installed Device Attribute (poly).vi"/>
				<Item Name="niModInst Get Installed Device Attribute (String).vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Get Installed Device Attribute (String).vi"/>
				<Item Name="niModInst Close Installed Devices Session.vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Close Installed Devices Session.vi"/>
				<Item Name="niDCPower Auto Zero.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Auto Zero.ctl"/>
				<Item Name="niDCPower Power Line Frequency.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Power Line Frequency.ctl"/>
				<Item Name="niDCPower Initialize With Channels.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Initialize With Channels.vi"/>
				<Item Name="niDCPower Configure Auto Zero.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Auto Zero.vi"/>
				<Item Name="niDCPower Configure Power Line Frequency.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Power Line Frequency.vi"/>
				<Item Name="niDCPower Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Reset.vi"/>
				<Item Name="niDCPower Reset Device.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Reset Device.vi"/>
				<Item Name="niScope timestamp type.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope timestamp type.ctl"/>
				<Item Name="niScope Read (poly).vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/niScope Read (poly).vi"/>
				<Item Name="niScope Multi Read WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/niScope Multi Read WDT.vi"/>
				<Item Name="niScope Abort.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Abort.vi"/>
				<Item Name="niScope Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/niScope Close.vi"/>
				<Item Name="niScope acquisition type.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope acquisition type.ctl"/>
				<Item Name="niScope Configure Vertical.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Vertical/niScope Configure Vertical.vi"/>
				<Item Name="niScope Configure Acquisition.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/niScope Configure Acquisition.vi"/>
				<Item Name="niScope Configure Chan Characteristics.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Vertical/niScope Configure Chan Characteristics.vi"/>
				<Item Name="niScope Configure Horizontal Timing.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Horizontal/niScope Configure Horizontal Timing.vi"/>
				<Item Name="niScope Configure Trigger (poly).vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger (poly).vi"/>
				<Item Name="niScope Configure Trigger Digital.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Digital.vi"/>
				<Item Name="niScope Configure Trigger Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Edge.vi"/>
				<Item Name="niScope Configure Trigger Immediate.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Immediate.vi"/>
				<Item Name="niScope Commit.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Utility/niScope Commit.vi"/>
				<Item Name="niScope Fetch (poly).vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch (poly).vi"/>
				<Item Name="niScope Multi Fetch WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch WDT.vi"/>
				<Item Name="niScope Acquisition Status.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Acquisition Status.vi"/>
				<Item Name="niScope Is Device Ready.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Utility/niScope Is Device Ready.vi"/>
				<Item Name="niScope Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/niScope Initialize.vi"/>
				<Item Name="niScope Initiate Acquisition.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Initiate Acquisition.vi"/>
				<Item Name="niSwitch Relay Action.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Relay Action.ctl"/>
				<Item Name="niSwitch Relay Control.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Relay Control.vi"/>
				<Item Name="niSwitch Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Close.vi"/>
				<Item Name="niSwitch Initialize With Topology.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Initialize With Topology.vi"/>
				<Item Name="niDCPower IVI Error Converter.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower IVI Error Converter.vi"/>
				<Item Name="niDCPower Configure Digital Edge Sequence Advance Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Digital Edge Sequence Advance Trigger.vi"/>
				<Item Name="niDCPower Digital Edge - Edge.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Digital Edge - Edge.ctl"/>
				<Item Name="niDCPower Configure Software Edge Sequence Advance Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Software Edge Sequence Advance Trigger.vi"/>
				<Item Name="niDCPower Configure Software Edge Source Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Software Edge Source Trigger.vi"/>
				<Item Name="niDCPower Configure Software Edge Start Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Software Edge Start Trigger.vi"/>
				<Item Name="niDCPower Disable Sequence Advance Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Disable Sequence Advance Trigger.vi"/>
				<Item Name="niDCPower Configure Software Edge Measure Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Software Edge Measure Trigger.vi"/>
				<Item Name="niDCPower Configure Digital Edge Pulse Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Digital Edge Pulse Trigger.vi"/>
				<Item Name="niDCPower Configure Software Edge Pulse Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Software Edge Pulse Trigger.vi"/>
				<Item Name="niDCPower Disable Pulse Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Disable Pulse Trigger.vi"/>
				<Item Name="niDCPower Current Limit Behavior.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Current Limit Behavior.ctl"/>
				<Item Name="niModInst Set Error.vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Set Error.vi"/>
				<Item Name="niModInst Get Installed Device Attribute (I32).vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Get Installed Device Attribute (I32).vi"/>
				<Item Name="niScope Read Cluster.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/niScope Read Cluster.vi"/>
				<Item Name="niScope LabVIEW Error.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Utility/niScope LabVIEW Error.vi"/>
				<Item Name="niScope Actual Record Length.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Horizontal/niScope Actual Record Length.vi"/>
				<Item Name="niScope Multi Read Cluster.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/niScope Multi Read Cluster.vi"/>
				<Item Name="niScope Actual Num Wfms.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Utility/niScope Actual Num Wfms.vi"/>
				<Item Name="niScope Read WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/niScope Read WDT.vi"/>
				<Item Name="niScope Get Session Reference.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Utility/niScope Get Session Reference.vi"/>
				<Item Name="niScope trigger source digital.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger source digital.ctl"/>
				<Item Name="niScope trigger source.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger source.ctl"/>
				<Item Name="niScope Configure Trigger Hysteresis.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Hysteresis.vi"/>
				<Item Name="niScope Configure Trigger Software.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Software.vi"/>
				<Item Name="niScope Configure Trigger Window.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Window.vi"/>
				<Item Name="niScope Configure Video Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Video Trigger.vi"/>
				<Item Name="niScope tv event.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope tv event.ctl"/>
				<Item Name="niScope polarity.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope polarity.ctl"/>
				<Item Name="niScope signal format.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope signal format.ctl"/>
				<Item Name="niScope Fetch Binary 8.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Binary 8.vi"/>
				<Item Name="niScope Fetch Error Chain.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Error Chain.vi"/>
				<Item Name="niScope Multi Fetch Binary 16.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Binary 16.vi"/>
				<Item Name="niScope Fetch Binary 16.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Binary 16.vi"/>
				<Item Name="niScope Multi Fetch Binary 32.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Binary 32.vi"/>
				<Item Name="niScope Fetch Binary 32.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Binary 32.vi"/>
				<Item Name="niScope Multi Fetch Binary 8.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Binary 8.vi"/>
				<Item Name="niScope Fetch Cluster.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Cluster.vi"/>
				<Item Name="niScope Multi Fetch Cluster.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Cluster.vi"/>
				<Item Name="niScope Fetch.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch.vi"/>
				<Item Name="niScope Multi Fetch.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch.vi"/>
				<Item Name="niScope Fetch WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch WDT.vi"/>
				<Item Name="niScope Fetch Complex Double.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Complex Double.vi"/>
				<Item Name="niScope Multi Fetch Complex Double.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Complex Double.vi"/>
				<Item Name="niScope Fetch Cluster Complex Double.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Cluster Complex Double.vi"/>
				<Item Name="niScope Multi Fetch Cluster Complex Double.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Cluster Complex Double.vi"/>
				<Item Name="niScope Fetch Complex WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Complex WDT.vi"/>
				<Item Name="niScope Multi Fetch Complex WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Complex WDT.vi"/>
				<Item Name="niSwitch IVI Error Converter.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch IVI Error Converter.vi"/>
				<Item Name="niScope export destinations.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope export destinations.ctl"/>
				<Item Name="niScope exportable signals.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope exportable signals.ctl"/>
				<Item Name="niScope Export Signal.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Export Signal.vi"/>
				<Item Name="niScope which signal.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope which signal.ctl"/>
			</Item>
			<Item Name="nidcpower_64.dll" Type="Document" URL="nidcpower_64.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niModInst_64.dll" Type="Document" URL="niModInst_64.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niswitch_64.dll" Type="Document" URL="niswitch_64.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niScope_64.dll" Type="Document" URL="niScope_64.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="nisyscfg.dll" Type="Document" URL="nisyscfg.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="DAQ.lvclass" Type="LVClass" URL="../Hardware Abstraction Layer/DAQ/DAQ.lvlibp/DAQ.lvclass"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Property Name="NI.SortType" Type="Int">2</Property>
			<Item Name="DAQ" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{EB809DF8-0FB4-4DBD-BD60-E6881E3BD293}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DAQ</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/DAQ</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{AED55413-F355-4D25-868B-95D2EDB1BE27}</Property>
				<Property Name="Bld_version.build" Type="Int">18</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">DAQ.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/DAQ/DAQ.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/DAQ</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{1396A95C-9E40-45D5-96A7-1690242FC3B5}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Hardware Abstraction Layer/DAQ/DAQ.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">National Instruments</Property>
				<Property Name="TgtF_enableDebugging" Type="Bool">true</Property>
				<Property Name="TgtF_fileDescription" Type="Str">DAQ</Property>
				<Property Name="TgtF_internalName" Type="Str">DAQ</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 National Instruments</Property>
				<Property Name="TgtF_productName" Type="Str">DAQ</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{10366F35-A238-4357-8DF7-7FCFE528AB80}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DAQ.lvlibp</Property>
			</Item>
		</Item>
	</Item>
</Project>
